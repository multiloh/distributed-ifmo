#include <stdio.h>
#include <unistd.h>

#include "ipc.h"
#include "procs.h"

int send(void * self, local_id dst, const Message * msg)
{
	int wd;
	local_id self_id;
	struct proc *self_proc = self;
	
	/* Getting write descriptor */
	self_id = self_proc->id;
	wd = self_proc->pipes_table[self_id][dst].wd;
	
	if (write(wd, msg, sizeof(msg->s_header) + msg->s_header.s_payload_len) < 0) {
        fprintf(stderr, "Message write error!");
        return 1;
    }
    
    return 0;
}

int send_multicast(void * self, const Message * msg)
{
	int i;
	struct proc *self_proc = self;
	
	for (i = 0; i < proc_cnt; i++)
	{
		if (i != self_proc->id)
		{
			send(self_proc, i, msg);
		}
	}
	
	return 0;
}

int receive(void * self, local_id from, Message * msg)
{
	int rd;
	local_id self_id;
	struct proc *self_proc = self;
	
	/* Getting read descriptor */
	self_id = self_proc->id;
	rd = self_proc->pipes_table[self_id][from].rd;
	
	if (read(rd, msg, sizeof(msg->s_header) + msg->s_header.s_payload_len) < 0) 
	{
        fprintf(stderr, "Message read error!");
        return 1;
    }
    
    return 0;
}

int receive_any(void * self, Message * msg)
{
	return 0;
}
