#include "common.h"
#include "logs.h"

void logs_open()
{
    pipeslog = fopen(pipes_log, "w");
    eventslog = fopen(events_log, "w");
}

void logs_close()
{
    fclose(pipeslog);
    fclose(eventslog);
}
