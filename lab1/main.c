#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "common.h"
#include "ipc.h"
#include "logs.h"
#include "pa1.h"
#include "procs.h"

Message message_buid(MessageType type, char *payload, int pl_length);

int main(int argc, char **argv)
{
	int opt;
	int i;
	int j;
	char *tmp;
	local_id id;
	
	struct proc proc;

    if (argc < 2) {
        return 1;
    }
    
    proc_cnt = 0;

    while ((opt = getopt(argc, argv, "p:")) != -1) {
        switch (opt) {
            case 'p':
                proc_cnt = strtol(optarg, &tmp, 0);

                if (tmp == optarg || *tmp != '\0') {
					fprintf(stderr, "-p: Wrong argument!\n");
                    return 1;
                }

                if (proc_cnt < 1) {
					fprintf(stderr, "Number of processes should be between 1 and 10.\n");
                    return 1;
                }
                
                proc_cnt++;

                break;
            case '?':
            default:
                return 1;
        }
    }
    
    logs_open();
    
    proc_pipestable_init(&proc);
    
    /* Process pool creation */
    for (id = 1; id < proc_cnt; id++)
    {
		pid_t ret = fork();
		
		if (ret < 0)
		{
			fprintf(stderr, "Child creation error: child #%d", id);
			return 1;
		}
		
		if (ret == 0)
		{
			fprintf(eventslog, "I am alive! %d\n", id);
			fflush(eventslog);
			proc.id = id;
			
			/* Closing unused pipes */
			for (i = 0; i < proc_cnt; i++) 
			{
				for (j = 0; j < proc_cnt; j++) 
				{
					if (i != id) 
					{
						close(proc.pipes_table[i][j].rd);
						close(proc.pipes_table[i][j].wd);
						
						fprintf(pipeslog, "Process %d: %d -> %d pipe closed\n", proc.id, i, j);
						fflush(pipeslog);
					}
				}
			}
			
			char payload[100];
			int pl_length;
			
			/* Sync */
			pl_length = sprintf(payload, log_started_fmt, proc.id, getpid(), getppid());
			Message start_msg = message_buid(STARTED, payload, pl_length);
			
			send_multicast(&proc, &start_msg);
			fprintf(eventslog, log_started_fmt, proc.id, getpid(), getppid());
			
			for (i = 1; i < proc_cnt; i++) 
			{
				if (i != proc.id) 
				{
					receive(&proc, i, &start_msg);
					if (start_msg.s_header.s_type != STARTED) 
					{
						fprintf(eventslog, "Receieved message with wrong type from %d\n", i);
						fflush(eventslog);
					}
				}
			}
			
			fprintf(eventslog, log_received_all_started_fmt, proc.id);
			
			/* Work */
			/* there is no work to do! */
			
			/* Suicide */
			memset(&payload[0], 0, sizeof(payload));
			
			pl_length = sprintf(payload, log_done_fmt, proc.id);
			Message done_msg = message_buid(DONE, payload, pl_length);
			
			send_multicast(&proc, &done_msg);
			fprintf(eventslog, log_done_fmt, proc.id);
			
			for (i = 1; i < proc_cnt; i++) 
			{
				if (i != proc.id) 
				{
					receive(&proc, i, &done_msg);
					if (done_msg.s_header.s_type != DONE) 
					{
						fprintf(eventslog, "Receieved message with wrong type from %d\n", i);
						fflush(eventslog);
					}
				}
			}
			
			fprintf(eventslog, log_received_all_done_fmt, proc.id);
			
			exit(0);
		}
	}
	
	proc.id = PARENT_ID;
	
	/* Closing unused parent's pipes */
	for (i = 0; i < proc_cnt; i++) 
	{
		for (j = 0; j < proc_cnt; j++) 
		{
			if (i != proc.id) 
			{
				close(proc.pipes_table[i][j].rd);
				close(proc.pipes_table[i][j].wd);
				
				fprintf(pipeslog, "Process %d: %d -> %d pipe closed\n", proc.id, i, j);
				fflush(pipeslog);
			}
		}
	}
	
	while (wait(NULL) > 0) ;
	
	logs_close();
	
	return 0;
}

Message message_buid(MessageType type, char *payload, int pl_length)
{
	int i;
	Message msg;
	
	msg.s_header.s_magic = MESSAGE_MAGIC;
	msg.s_header.s_payload_len = pl_length;
	msg.s_header.s_type = type;
	msg.s_header.s_local_time = (timestamp_t) time(NULL);
	
	for (i = 0; i < pl_length; i++) {
        msg.s_payload[i] = payload[i];
    }
	
	return msg;
}
