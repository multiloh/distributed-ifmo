#ifndef __LOGS__H
#define __LOGS__H

#include <stdio.h>

FILE *pipeslog;
FILE *eventslog;

void logs_open();
void logs_close();

#endif /* __LOGS__H */
