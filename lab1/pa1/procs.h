#ifndef __PROCS__H
#define __PROCS__H

#include "ipc.h"

int proc_cnt;

struct proc_pipe 
{
	int rd; 
	int wd;
};

struct proc
{
	local_id id;
	struct proc_pipe **pipes_table;
};

void proc_pipestable_init(struct proc *proc);
void proc_pipestable_free(struct proc *proc);

#endif /* __PROCS__H */
