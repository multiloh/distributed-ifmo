#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "logs.h"
#include "procs.h"

void proc_pipestable_init(struct proc *proc) 
{
	int i;
	int j;
	
	proc->pipes_table = malloc(sizeof(struct proc_pipe*) * proc_cnt);
	
	for (i = 0; i < proc_cnt; i++)
	{
		proc->pipes_table[i] = malloc(sizeof(struct proc_pipe) * proc_cnt);
	}
	
	for (i = 0; i < proc_cnt; i++)
	{
		/* Pipes "i -> i" are useless */
		for (j = i + 1; j < proc_cnt; j++)
		{
			int ij_pipe[2];
			int ji_pipe[2];
			
			if (pipe(ij_pipe) < 0)
			{
				fprintf(stderr, "Pipe creation error: %d -> %d\n", i, j);
				exit(1);
			}
			
			if (pipe(ji_pipe) < 0)
			{
				fprintf(stderr, "Pipe creation error: %d -> %d\n", j, i);
				exit(1);
			}
			
			proc->pipes_table[i][j].rd = ij_pipe[0];
			proc->pipes_table[i][j].wd = ji_pipe[1];
			
			proc->pipes_table[j][i].rd = ji_pipe[0];
			proc->pipes_table[j][i].wd = ij_pipe[1];
			
			fprintf(pipeslog, "%d -> %d connection. read desc.: %d, write desc.: %d\n", i, j, ij_pipe[0], ij_pipe[1]);
			fflush(pipeslog);
			fprintf(pipeslog, "%d -> %d connection. read desc.: %d, write desc.: %d\n", j, i, ji_pipe[0], ji_pipe[1]);
			fflush(pipeslog);
		}
	}
}
