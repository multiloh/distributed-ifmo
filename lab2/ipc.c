#define _XOPEN_SOURCE 
#define _XOPEN_SOURCE_EXTENDED

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "ipc.h"
#include "procs.h"

int send(void * self, local_id dst, const Message * msg)
{
	int wd;
	local_id self_id;
	struct proc *self_proc = self;
	
	/* Getting write descriptor */
	self_id = self_proc->id;
	wd = self_proc->pipes_table[self_id][dst].wd;
	
	if (write(wd, msg, sizeof(MessageHeader) + msg->s_header.s_payload_len) < 0) {
        fprintf(stderr, "Message write error!");
        return 1;
    }
    
    return 0;
}

int send_multicast(void * self, const Message * msg)
{
	int i;
	struct proc *self_proc = self;
	
	for (i = 0; i < proc_cnt; i++)
	{
		if (i != self_proc->id)
		{
			send(self_proc, i, msg);
		}
	}
	
	return 0;
}

int receive(void * self, local_id from, Message * msg)
{
	int rd;
	int readed = 0;
	local_id self_id;
	struct proc *self_proc = self;
	
	/* Getting read descriptor */
	self_id = self_proc->id;
	rd = self_proc->pipes_table[self_id][from].rd;
	
    do
    {
        ssize_t len = read(rd, msg, sizeof(Message));
        if (len == -1 && errno == EAGAIN)
        {
            usleep(500);
            continue;
        }
        else
        {
            readed = 1;
            break;
        }
    } while(!readed);
    
    return 0;
}

int receive_any(void * self, Message * msg)
{
	int rd;
	int readed = 0;
	int attempts = 0;
	int i;
	local_id self_id;
	struct proc *self_proc = self;
	
	self_id = self_proc->id;
	
	do
    {
        for (i = 0; i < proc_cnt; i++)
        {
            if (i == self_id) 
            { 
				continue; 
			}
			
			rd = self_proc->pipes_table[self_id][i].rd;
            ssize_t len = read(rd, msg, sizeof(Message));
            if ((len == -1 && errno == EAGAIN) || len == 0)
            {
				attempts++;
                continue;
            }
            else
            {
                readed = 1;
                return 0;
            }
        }
        usleep(500);
    }
    while (!readed && attempts < 5);
		
	return 1;
}
