#ifndef __PROCS__H
#define __PROCS__H

#include "banking.h"
#include "ipc.h"

int proc_cnt;

struct proc_pipe 
{
	int rd; 
	int wd;
};

struct proc
{
	local_id id;
	struct proc_pipe **pipes_table;
};

struct service_proc
{
	BalanceState balance;
    BalanceHistory history;
};

void proc_pipestable_init(struct proc *proc);
void proc_pipestable_free(struct proc *proc);

#endif /* __PROCS__H */
