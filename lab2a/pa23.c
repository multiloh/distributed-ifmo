#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <fcntl.h>

#include "banking.h"
#include "common.h"
#include "ipc.h"
#include "logs.h"
#include "pa2345.h"
#include "procs.h"

#define ANY_CHILD -1
#define READ_FD 0
#define WRITE_FD 1

static int** r_fds;      /* Read FDs */
static int** w_fds;      /* Write FDs */
static balance_t balance;
static BalanceHistory history;

Message message_build(MessageType type, char *payload, int pl_length);

void close_unused_fd(local_id process_id);
int run_child(int* write_fd, int* read_fd, local_id child_id);
int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type);
int do_transactions(int* write_fd, int* read_fd, local_id child_id);
void close_descriptors();
void free_descriptors();
void suicide();
int run_parent();

void transfer(void *self, local_id src, local_id dst, balance_t amount)
{

    Message req;
    Message resp;
    TransferOrder order;

    order.s_src = src;
    order.s_dst = dst;
    order.s_amount = amount;
 
    req = message_build(TRANSFER, (char*) &order, sizeof(order)); 
    
    send(self, src, &req);

    do
    {
        receive(self, dst, &resp);
    }
    while (resp.s_header.s_type != ACK);
}

int main(int argc, char **argv)
{
    balance_t *balances;
    int fields[2];
    int flags;
    
    int opt;
	int i;
    int j;
	char *tmp;
    
    while ((opt = getopt(argc, argv, "p:")) != -1) {
        switch (opt) {
            case 'p':
                proc_cnt = strtol(optarg, &tmp, 0);

                if (tmp == optarg || *tmp != '\0') {
					fprintf(stderr, "-p: Wrong argument!\n");
                    return 1;
                }

                if (proc_cnt < 1) {
					fprintf(stderr, "Number of processes should be between 1 and 10.\n");
                    return 1;
                }
                
                if (argc < (optind + proc_cnt)) {
					fprintf(stderr, "You should specify balances for every account.\n");
                    return 1;
                }
                
                balances = malloc(sizeof(balance_t) * proc_cnt);
                
                for (i = 0; i < proc_cnt; i++)
                {
					balances[i] = atoi(argv[optind]);
					
					optind++;
				}

                break;
            case '?':
            default:
                return 1;
        }
    }    

    logs_open();

    //Creating files descriptors
    r_fds = malloc(sizeof(int*) * (proc_cnt + 1));
    w_fds = malloc(sizeof(int*) * (proc_cnt + 1));
    for (i = 0; i < proc_cnt + 1; i++)
    {
        r_fds[i] = malloc(sizeof(int) * (proc_cnt + 1));
        w_fds[i] = malloc(sizeof(int) * (proc_cnt + 1));
    }

    //Creating files descriptors
    for (i = 0; i < proc_cnt + 1; i++)
    {
        for (j = 0; j < proc_cnt + 1; j++)
        {
            if (i == j) { continue; }
            if (0 == pipe(fields))
            {
                flags = fcntl(fields[READ_FD], F_GETFL,0);
                fcntl(fields[READ_FD], F_SETFL, flags | O_NONBLOCK);
                flags = fcntl(fields[WRITE_FD], F_GETFL,0);
                fcntl(fields[WRITE_FD], F_SETFL, flags | O_NONBLOCK);
                r_fds[j][i] = fields[READ_FD];
                w_fds[i][j] = fields[WRITE_FD];
                fprintf(pipeslog, "%d-->%d %d/%d\n", i,j, fields[WRITE_FD], fields[READ_FD]);
            }
            else
            {
                free_descriptors();
                exit(1);
            }
        }
    }
    //Saving pipes to log
    //fclose(pipeslog);

    //Starting to create children
    pid_t *child_processes = malloc(sizeof(pid_t) * proc_cnt);
    for (int i = 0; i < proc_cnt; i++)
    {
        child_processes[i] = fork();
        int childId = i + 1;
        //Exit if failed to fork
        if (-1 == child_processes[i] )
        {
                free_descriptors();
                exit(1);
        }
        //Child logic here
        if (0 ==  child_processes[i])
        {
            balance = balances[i];
            timestamp_t time = get_physical_time();
            history.s_id = childId;
            history.s_history_len = time + 1;
            for (int i = 0; i <= time; i++)
            {
                history.s_history[i].s_balance = (i == time ? balance : 0);
                history.s_history[i].s_balance_pending_in = 0;
                history.s_history[i].s_time = i;
            }
            //close other child FD
            close_unused_fd((local_id)childId);
            int result = run_child(w_fds[childId], r_fds[childId], (local_id)childId);
            suicide();
            exit(result);
        }
    }
    //Parent logic here
    return run_parent();
}

void close_unused_fd(local_id process_id)
{
    for (int i=0; i< proc_cnt + 1; i++)
    {
        if (i == process_id) {continue;}
        for (int j=0; j < proc_cnt + 1; j++)
        {
            if (i == j) {continue;}
            close(r_fds[i][j]);
            close(w_fds[i][j]);
        }
    }
}

int run_parent()
{
    int result = 0;
    int child_status;
    //close other FD
    close_unused_fd(PARENT_ID);

    struct proc proc_state;
    //Creating message
    Message msg;
    proc_state.write_fd = w_fds[PARENT_ID];
    proc_state.read_fd = r_fds[PARENT_ID];
    proc_state.proc_cnt = proc_cnt;
    proc_state.id = PARENT_ID;
    int started_count = 0;
    int done_count = 0;
    
    //waiting for all started
    do
    {
        receive_any(&proc_state, &msg);
        if (msg.s_header.s_type == STARTED)
        {
            started_count++;
        }
    }
    while (started_count < proc_cnt);

    //all started event logging
    char message[MAX_PAYLOAD_LEN];
    timestamp_t time = get_physical_time();
    sprintf(message, log_received_all_started_fmt, time, PARENT_ID);
    write(eventslog, message, strlen(message));
    printf("%s", message);

    //banking
    bank_robbery(&proc_state, proc_cnt);

    //send stop to children
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = STOP;
    msg.s_header.s_local_time = get_physical_time();
    msg.s_header.s_payload_len = 0;
    send_multicast(&proc_state, &msg);

    int sender_id;
    //waiting for all done
    do
    {
        sender_id = receive_any(&proc_state, &msg);
        if (msg.s_header.s_type == DONE)
        {
            done_count++;
        }
    }
    while (done_count < proc_cnt);
    time = get_physical_time();
    sprintf(message, log_received_all_done_fmt, time, PARENT_ID);
    write(eventslog, message, strlen(message));
    printf("%s", message);

    //getting history
    AllHistory allHistory;
    allHistory.s_history_len = proc_cnt;
    int history_count = 0;
    do
    {
        sender_id = receive_any(&proc_state, &msg);
        if (msg.s_header.s_type == BALANCE_HISTORY)
        {
            memcpy(&allHistory.s_history[sender_id-1],msg.s_payload, sizeof(BalanceHistory)); 
            history_count++;
        }
    }
    while (history_count < proc_cnt);
    print_history(&allHistory);
    //waiting for childrens death
    do
    {
        waitpid(ANY_CHILD, &child_status, 0);
        if (0 != child_status) 
        {
            result = 1;
        }
        proc_cnt--;
    }
    while (proc_cnt > 0);
    suicide();
    return result;
}



int run_child(int* write_fd, int* read_fd, local_id child_id)
{
    synchronize(write_fd, read_fd, child_id, STARTED);
    do_transactions(write_fd, read_fd, child_id);
    return 0;
}

int synchronize(int* write_fd, int* read_fd, local_id child_id, MessageType m_type)
{
    if (NULL == write_fd || NULL == read_fd) { return 1; }

    struct proc proc_state;
    //Creating message
    Message msg;
    msg.s_header.s_local_time = get_physical_time();
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = m_type;
    proc_state.write_fd = write_fd;
    proc_state.read_fd = read_fd;
    proc_state.proc_cnt = proc_cnt;
    proc_state.id = child_id;

    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_started_fmt, get_physical_time(), child_id, getpid(), getppid(), balance);   
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_done_fmt, get_physical_time(), child_id, balance); 
    }
    msg.s_header.s_payload_len = strlen(msg.s_payload);

    send_multicast(&proc_state, &msg);
    write(eventslog, msg.s_payload, msg.s_header.s_payload_len);
    printf("%s", msg.s_payload);

    //Finish of synchronisation
    for (int i=0; i<proc_cnt-1; ++i)
    {
        receive_any(&proc_state, &msg);
    }
    if (STARTED == m_type)
    {
        sprintf(msg.s_payload, log_received_all_started_fmt, get_physical_time(), child_id); 
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    else if (DONE == m_type)
    {
        sprintf(msg.s_payload, log_received_all_done_fmt, get_physical_time(), child_id);
        write(eventslog, msg.s_payload, strlen(msg.s_payload));
        printf("%s", msg.s_payload);
    }
    return 0;
}

int do_transactions(int* write_fd, int* read_fd, local_id child_id)
{
    Message msg;
    TransferOrder t_order;
    struct proc proc_state;
    
    proc_state.write_fd = write_fd;
    proc_state.read_fd = read_fd;
    proc_state.proc_cnt = proc_cnt;
    proc_state.id = child_id;
    
    while(1)
    {
        receive_any(&proc_state, &msg);
        timestamp_t current_time = get_physical_time();
        //Filling in history to current moment
        for (uint8_t i = history.s_history_len; i <= current_time; i++)
        {
            history.s_history[i].s_balance = balance;
            history.s_history[i].s_balance_pending_in = 0;
            history.s_history[i].s_time = i;
        }
        history.s_history_len = current_time + 1;
        if (TRANSFER == msg.s_header.s_type)
        {
            memcpy(&t_order, &msg.s_payload, msg.s_header.s_payload_len);
            //I must send
            if (child_id == t_order.s_src) 
            {
                //Logging of log_transfer_out_fmt
                char message[MAX_PAYLOAD_LEN];
                sprintf(message, log_transfer_in_fmt, get_physical_time(), child_id, t_order.s_amount, t_order.s_dst); 
                write(eventslog, message, strlen(message));
                printf("%s", message);

                balance -= t_order.s_amount;
                msg.s_header.s_local_time = current_time;
                send(&proc_state, t_order.s_dst, &msg);
            }
            //I must receive
            else 
            {
                //Logging of log_transfer_in_fmt
                char message[MAX_PAYLOAD_LEN];
                sprintf(message, log_transfer_out_fmt, get_physical_time(), child_id, t_order.s_amount, t_order.s_src); 
                write(eventslog, message, strlen(message));
                printf("%s", message);

                balance += t_order.s_amount;
                msg.s_header.s_local_time = current_time;
                msg.s_header.s_magic = MESSAGE_MAGIC;
                msg.s_header.s_type = ACK;
                msg.s_header.s_payload_len = 0;
                send(&proc_state, 0, &msg);
            }
            //Fixing last history balance record
            history.s_history[current_time].s_balance = balance;
        }
        else if (STOP == msg.s_header.s_type)
        {
            //FIXME
            synchronize(write_fd, read_fd, child_id, DONE);
            Message history_msg;
            history_msg.s_header.s_local_time = get_physical_time();
            history_msg.s_header.s_type = BALANCE_HISTORY;
            ssize_t size = sizeof(BalanceHistory) - sizeof(BalanceState)*(MAX_T + 1 - history.s_history_len);
            memcpy(history_msg.s_payload, &history, size);
            history_msg.s_header.s_payload_len = size;
            send(&proc_state, 0, &history_msg);
            return 0;
        }
    }
    return 0;
}

void suicide()
{
    free_descriptors();
    logs_close();
}

void free_descriptors()
{
    close_descriptors();
    for (int i=0; i<proc_cnt + 1; ++i)
    {
        free(w_fds[i]);
        free(r_fds[i]);
    }
    free(r_fds);
    free(w_fds);
}

void close_descriptors()
{
    for (int i=0; i<proc_cnt + 1 ; ++i)
    {
        for (int j=0; j < proc_cnt + 1; ++j)
        {
            close(r_fds[i][j]);
            close(w_fds[i][j]);
        }
    }

}

Message message_build(MessageType type, char *payload, int pl_length)
{
	int i;
	Message msg;
	
	msg.s_header.s_magic = MESSAGE_MAGIC;
	msg.s_header.s_payload_len = pl_length;
	msg.s_header.s_type = type;
	msg.s_header.s_local_time = get_physical_time();
	
	for (i = 0; i < pl_length; i++) {
        msg.s_payload[i] = payload[i];
    }
	
	return msg;
}

