#ifndef __PROCS__H
#define __PROCS__H

#include "ipc.h"

int proc_cnt;

struct proc {
    int* read_fd;
    int* write_fd;
    int proc_cnt;
    local_id id;
} MessageAttributes;

void proc_pipestable_init(struct proc *proc);
//void proc_pipestable_free(struct proc *proc);

#endif /* __PROCS__H */
