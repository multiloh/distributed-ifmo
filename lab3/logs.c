#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "common.h"
#include "logs.h"

void logs_open()
{
    pipeslog = fopen(pipes_log, "w");
    eventslog = open(events_log, O_CREAT | O_TRUNC |  O_RDWR | O_APPEND);
}

void logs_close()
{
    fclose(pipeslog);
    close(eventslog);
}
