#include "banking.h"
#include "ipc.h"
#include "ltime.h"

timestamp_t get_lamport_time()
{
	return lamportTime;
}

void set_lamport_time(timestamp_t ltime)
{
	lamportTime = ltime;
	return;
}

timestamp_t lcompare(timestamp_t a, timestamp_t b)
{
	if (a > b)
	{
		return a;
	}
	else 
	{
		return b;
	}
}
