#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "banking.h"
#include "common.h"
#include "ipc.h"
#include "logs.h"
#include "ltime.h"
#include "pa2345.h"
#include "procs.h"

Message message_build(MessageType type, char *payload, int pl_length);
void service_proc_suicide(struct proc *self, struct service_proc *bank_acc);

int main(int argc, char * argv[])
{
	int opt;
	int i;
	int j;
	char *tmp;
	char buf[256];
	local_id id;
	balance_t *balances;
	
	struct proc proc;
	
	if (argc < 2) {
        return 1;
    }
    
    proc_cnt = 0;

    while ((opt = getopt(argc, argv, "p:")) != -1) {
        switch (opt) {
            case 'p':
                proc_cnt = strtol(optarg, &tmp, 0);

                if (tmp == optarg || *tmp != '\0') {
					fprintf(stderr, "-p: Wrong argument!\n");
                    return 1;
                }

                if (proc_cnt < 1) {
					fprintf(stderr, "Number of processes should be between 1 and 10.\n");
                    return 1;
                }
                
                if (argc < (optind + proc_cnt)) {
					fprintf(stderr, "You should specify balances for every account.\n");
                    return 1;
                }
                
                proc_cnt++;
                
                balances = malloc(sizeof(balance_t) * proc_cnt);
                
                for (i = 1; i < proc_cnt; i++)
                {
					balances[i] = atoi(argv[optind]);
					
					optind++;
				}

                break;
            case '?':
            default:
                return 1;
        }
    }
    
    logs_open();

    set_lamport_time(0);
	
	proc_pipestable_init(&proc);
	
	/* Process pool creation */
    for (id = 1; id < proc_cnt; id++)
    {
		pid_t ret = fork();
		
		if (ret < 0)
		{
			fprintf(stderr, "Child creation error: child #%d", id);
			return 1;
		}
		
		if (ret == 0)
		{
			sprintf(buf, "I am alive! %d\n", id);
			write(eventslog, buf, strlen(buf));
			
			proc.id = id;
			
			struct service_proc bank_acc;
				
			bank_acc.balance.s_balance = balances[id];
            bank_acc.balance.s_time = get_lamport_time();
            bank_acc.balance.s_balance_pending_in = 0;

            bank_acc.history.s_id = id;
            bank_acc.history.s_history_len = bank_acc.balance.s_time + 1;
            for (int i = 0; i <= bank_acc.balance.s_time; i++)
            {
                bank_acc.history.s_history[i].s_balance = (i == bank_acc.balance.s_time ? bank_acc.balance.s_balance : 0);
                bank_acc.history.s_history[i].s_balance_pending_in = 0;
                bank_acc.history.s_history[i].s_time = i;
            }
			
			/* Closing unused pipes */
			for (i = 0; i < proc_cnt; i++) 
			{
				for (j = 0; j < proc_cnt; j++) 
				{
					if (i != id) 
					{
						close(proc.pipes_table[i][j].rd);
						close(proc.pipes_table[i][j].wd);
						
						fprintf(pipeslog, "Process %d: %d -> %d pipe closed\n", proc.id, i, j);
						fflush(pipeslog);
					}
				}
			}
			
			char payload[100];
			int pl_length;
			
			/* Sync */
			pl_length = sprintf(payload, log_started_fmt, get_lamport_time(), proc.id, getpid(), getppid(), bank_acc.balance.s_balance);

			bank_acc.balance.s_time = get_lamport_time();
			bank_acc.history.s_history[get_lamport_time()] = bank_acc.balance;

			set_lamport_time(get_lamport_time() + 1);

			//printf("%d: slave %d has s_balance %d\n", get_lamport_time(), proc.id, bank_acc.balance.s_balance);

			//bank_acc.history.s_history[get_lamport_time()] = bank_acc.balance;

			Message start_msg = message_build(STARTED, payload, pl_length);
			
			send_multicast(&proc, &start_msg);
			write(eventslog, payload, strlen(payload));
			
			for (i = 1; i < proc_cnt; i++) 
			{
				if (i != proc.id) 
				{
					receive(&proc, i, &start_msg);

					//for(j = get_lamport_time(); j <= start_msg.s_header.s_local_time; j++)
					//{
					//	bank_acc.balance.s_time = j;
					//	bank_acc.history.s_history[bank_acc.history.s_history_len++] = bank_acc.balance;
					//}

					//set_lamport_time(lcompare(start_msg.s_header.s_local_time, get_lamport_time()) + 1);
					
					//printf("kok1\n");
					//for (i = 0; i < bank_acc.history.s_history_len; i++)
					//{
					//	printf("%d: bank %d has s_balance %d\n", i, proc.id, bank_acc.balance.s_balance);
					//}

					for(j = bank_acc.history.s_history_len; j <= get_lamport_time(); j++)
					{
						bank_acc.balance.s_time = j;
						bank_acc.history.s_history[bank_acc.history.s_history_len++] = bank_acc.balance;
					}

					//printf("kok2\n");
					//for (i = 0; i < bank_acc.history.s_history_len; i++)
					//{
					//	printf("%d: bank %d has s_balance %d\n", i, proc.id, bank_acc.balance.s_balance);
					//}

					set_lamport_time(lcompare(start_msg.s_header.s_local_time, get_lamport_time()) + 1);

					if (start_msg.s_header.s_type != STARTED) 
					{
						memset(buf, 0, strlen(buf));
						sprintf(buf, "Receieved message with wrong type from %d\n", i);
						write(eventslog, buf, strlen(buf));
					}
				}
			}

			memset(buf, 0, strlen(buf));
			sprintf(buf, log_received_all_started_fmt, get_lamport_time(), proc.id);
			write(eventslog, buf, strlen(buf));
						
			/* Work */
			Message msg;
			while(1)
			{				
				if (!receive_any(&proc, &msg)) 
				{		
					set_lamport_time(lcompare(msg.s_header.s_local_time, get_lamport_time()) + 1);

					for(j = bank_acc.history.s_history_len; j <= get_lamport_time(); j++)
					{
						bank_acc.balance.s_time = j;
						bank_acc.balance.s_balance_pending_in = 0;
						//printf("%d: slave %d gone to shitty FOR with s_balance %d\n", get_lamport_time(), proc.id, bank_acc.balance.s_balance);
						bank_acc.history.s_history[bank_acc.history.s_history_len++] = bank_acc.balance;
					}

					//set_lamport_time(lcompare(msg.s_header.s_local_time, get_lamport_time()) + 1);

					switch (msg.s_header.s_type) {
						case STOP:			/* Suicide */
							service_proc_suicide(&proc, &bank_acc);
							
							BalanceHistory *history = &bank_acc.history;

							set_lamport_time(get_lamport_time() + 1);

							Message hist = message_build(BALANCE_HISTORY, (char*) history, sizeof(BalanceHistory));

							send(&proc, PARENT_ID, &hist);
		
							exit(0);
							break;
						case TRANSFER:
						{
							TransferOrder *order = (TransferOrder*) msg.s_payload;
							if (order->s_src == proc.id) 
							{
								bank_acc.balance.s_balance -= order->s_amount;
								bank_acc.balance.s_balance_pending_in = order->s_amount;
								bank_acc.balance.s_time = get_lamport_time();
								
								bank_acc.history.s_history[get_lamport_time()] = bank_acc.balance;

								set_lamport_time(get_lamport_time() + 1);
								msg.s_header.s_local_time = get_lamport_time();

								send(&proc, order->s_dst, &msg);
								memset(buf, 0, strlen(buf));
								sprintf(buf, log_transfer_out_fmt, get_lamport_time(), proc.id, order->s_amount, order->s_dst);
								write(eventslog, buf, strlen(buf));
							} 
							else 
							{
								if (order->s_dst != proc.id) 
								{
									memset(buf, 0, strlen(buf));
									sprintf(buf, "Receieved weird message from %d\n", order->s_dst);
									write(eventslog, buf, strlen(buf));
									
									break;
								}

								bank_acc.balance.s_balance += order->s_amount;
								bank_acc.balance.s_balance_pending_in = 0;
								//bank_acc.balance.s_balance_pending_in = order->s_amount;
								bank_acc.balance.s_time = get_lamport_time();
								
								bank_acc.history.s_history[get_lamport_time()] = bank_acc.balance;
								
								memset(buf, 0, strlen(buf));
								sprintf(buf, log_transfer_in_fmt, get_lamport_time(), proc.id, order->s_amount, order->s_src);
								write(eventslog, buf, strlen(buf));
								
								set_lamport_time(get_lamport_time() + 1);

								Message ack = message_build(ACK, NULL, 0);
								send(&proc, PARENT_ID , &ack);
							}
							bank_acc.balance.s_time = get_lamport_time();
							bank_acc.history.s_history[bank_acc.history.s_history_len++] = bank_acc.balance;

							break;
						}
						default:
							break;
					}
				}
				else
				{									
					//bank_acc.balance.s_time = get_lamport_time();
					//printf("%d: slave %d gone to shitty ELSE with s_balance %d\n", get_lamport_time(), proc.id, bank_acc.balance.s_balance);
					//bank_acc.history.s_history[bank_acc.history.s_history_len++] = bank_acc.balance;
				}
			}
			
			exit(0);
		}
	}
	
	proc.id = PARENT_ID;
	
	/* Closing unused parent's pipes */
	for (i = 0; i < proc_cnt; i++) 
	{
		for (j = 0; j < proc_cnt; j++) 
		{
			if (i != proc.id) 
			{
				close(proc.pipes_table[i][j].rd);
				close(proc.pipes_table[i][j].wd);
				
				fprintf(pipeslog, "Process %d: %d -> %d pipe closed\n", proc.id, i, j);
				fflush(pipeslog);
			}
		}
	}
	
	/* Waiting for services STARTED messages */
	Message tmp_msg = message_build(STARTED, NULL, 0);
	
	for (int i = 1; i < proc_cnt; i++) 
	{
        if (i != proc.id) 
        {
            receive(&proc, i, &tmp_msg);
            set_lamport_time(lcompare(tmp_msg.s_header.s_local_time, get_lamport_time()) + 1);
            if (tmp_msg.s_header.s_type != STARTED) 
            {
                memset(buf, 0, strlen(buf));
				sprintf(buf, "Receieved message with wrong type from %d\n", i);
				write(eventslog, buf, strlen(buf));   
            }
        }
    }
    
    bank_robbery(&proc, proc_cnt - 1);
    
    /* Waiting for services responce for STOP messages */
    set_lamport_time(get_lamport_time() + 1);
    tmp_msg = message_build(STOP, NULL, 0);
    send_multicast(&proc, &tmp_msg);
	
	for (int i = 1; i < proc_cnt; i++) 
	{
        if (i != proc.id) 
        {
            receive(&proc, i, &tmp_msg);
            set_lamport_time(lcompare(tmp_msg.s_header.s_local_time, get_lamport_time()) + 1);
            if (tmp_msg.s_header.s_type != DONE) 
            {
                memset(buf, 0, strlen(buf));
				sprintf(buf, "Receieved message with wrong type from %d\n", i);
				write(eventslog, buf, strlen(buf));    
            }
        }
    }
    
    memset(buf, 0, strlen(buf));
	sprintf(buf, log_received_all_done_fmt, get_lamport_time(), proc.id);
	write(eventslog, buf, strlen(buf));
	
	Message msg;
	AllHistory allHistory;

	for (i = 1; i <= proc_cnt - 1; i++) {
		receive(&proc, i, &msg);
		set_lamport_time(lcompare(msg.s_header.s_local_time, get_lamport_time()) + 1);
		allHistory.s_history[i - 1] = *((BalanceHistory*)msg.s_payload);
	}

	allHistory.s_history_len = proc_cnt - 1;
	print_history(&allHistory);
	
	while (wait(NULL) > 0) ;

	logs_close();

    return 0;
}

Message message_build(MessageType type, char *payload, int pl_length)
{
	int i;
	Message msg;
	
	msg.s_header.s_magic = MESSAGE_MAGIC;
	msg.s_header.s_payload_len = pl_length;
	msg.s_header.s_type = type;
	msg.s_header.s_local_time = get_lamport_time();
	
	for (i = 0; i < pl_length; i++) {
        msg.s_payload[i] = payload[i];
    }
	
	return msg;
}

void service_proc_suicide(struct proc *self, struct service_proc *bank_acc)
{
	char payload[100];
	char buf[256];
	int pl_length;	
	int i;
	int j;
	
	pl_length = sprintf(payload, log_done_fmt, get_lamport_time(), self->id, bank_acc->balance.s_balance);

	set_lamport_time(get_lamport_time() + 1);

	Message done_msg = message_build(DONE, payload, pl_length);
	
	send_multicast(self, &done_msg);
	
	write(eventslog, payload, strlen(payload));
	
	for (i = 1; i < proc_cnt; i++) 
	{
		if (i != self->id) 
		{
			receive(self, i, &done_msg);

			for(j = get_lamport_time(); j <= done_msg.s_header.s_local_time; j++)
			{
				bank_acc->balance.s_time = j;
				bank_acc->history.s_history[bank_acc->history.s_history_len++] = bank_acc->balance;
			}

			set_lamport_time(lcompare(done_msg.s_header.s_local_time, get_lamport_time()) + 1);

			if (done_msg.s_header.s_type != DONE) 
			{
				memset(buf, 0, strlen(buf));
				sprintf(buf, "Receieved message with wrong type from %d\n", i);
				write(eventslog, buf, strlen(buf));
			}
		}
	}
	
	memset(buf, 0, strlen(buf));
	sprintf(buf, log_received_all_done_fmt, get_lamport_time(), self->id);
	write(eventslog, buf, strlen(buf));
}
