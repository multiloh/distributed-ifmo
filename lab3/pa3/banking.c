#include "banking.h"
#include "ipc.h"
#include "ltime.h"

void transfer(void * parent_data, local_id src, local_id dst, balance_t amount)
{
	int i;
    Message req;
    Message resp;
	TransferOrder order;
	
	order.s_src = src;
	order.s_dst = dst;
	order.s_amount = amount;

	set_lamport_time(get_lamport_time() + 1);
	
	req.s_header.s_magic = MESSAGE_MAGIC;
	req.s_header.s_payload_len = sizeof(TransferOrder);
	req.s_header.s_type = TRANSFER;
	req.s_header.s_local_time = get_lamport_time();
	
	char *payload = (char*) &order;
	
	for (i = 0; i < req.s_header.s_payload_len; i++) {
        req.s_payload[i] = payload[i];
    }
	
	send(parent_data, src, &req);

	while(1)
	{
		receive(parent_data, dst, &resp);

		set_lamport_time(lcompare(resp.s_header.s_local_time, get_lamport_time()) + 1);
		
		if (resp.s_header.s_type == ACK)
		{
			return;
		}
	}
}
