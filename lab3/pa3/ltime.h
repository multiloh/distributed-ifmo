#ifndef __LTIME__H
#define __LTIME__H

#include "ipc.h"

static timestamp_t lamportTime;

void set_lamport_time(timestamp_t ltime);
timestamp_t lcompare(timestamp_t a, timestamp_t b);

#endif /* __LTIME__H */
