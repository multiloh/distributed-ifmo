#define _XOPEN_SOURCE 
#define _XOPEN_SOURCE_EXTENDED

#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "ipc.h"
#include "logs.h"
#include "ltime.h"
#include "procs.h"

int send(void *self, local_id dst, const Message *msg)
{
	int from = id;
	int to = dst;
	int pipe = 3;

	struct pipes_t *pipes = (struct pipes_t*) self;

  	if(dst < id)
  	{
    	from = dst;
    	to = id;
    	pipe = 1;
	}

    if(write(pipes[getOffset(from) + to - from - 1].pipes[pipe], msg, sizeof(MessageHeader) + msg->s_header.s_payload_len) < 0)
    {
    	fprintf(stderr, "Message write error!");
        return 1;
  	}

	fflush(pipeslog);

	return 0;
}

int send_multicast(void *self, const Message *msg)
{
    int i;
	
	for (i = 0; i <= proc_cnt; i++)
	{
		if (i != id)
		{
			send(self, i, msg);
		}
	}
	
	return 0;
}

int receive(void *self, local_id from, Message *msg)
{
    int _from = from;
    int to = id;
    int pipe = 2;
    int tmp = 0;

  	struct pipes_t *pipes = (struct pipes_t*) self;

    if (_from > to)
    {
    	pipe = 0;
    	_from = id;
    	to = from;
	}
  
  	while((tmp = read(pipes[getOffset(_from) + to - _from - 1].pipes[pipe], &msg->s_header, sizeof(MessageHeader))) == 0 || errno == EAGAIN || errno == EWOULDBLOCK) 
  	{
  		errno = 0;
  	}

    fflush(pipeslog);

	r_time = msg->s_header.s_local_time;
    if (r_time > local_time) 
	{
    	local_time = r_time;
    }

    local_time++;
  
    return 0;
}

int receive_any(void *self, Message *msg)
{
    int pipe = 2; 
    int to = id;
    int from = 0;
    int i;
    int tmp;

    struct pipes_t *pipes = (struct pipes_t*) self;

    for(i = 0; i < proc_cnt + 1; i++) {
	    if (i == id) 
		{
	    	continue;
	    }

	    from = i;

	    if (from > to)
	    {
	    	pipe = 0;
	  		from = id;
	  		to = i;
		}

	    errno = 0;

	    if ((tmp = read(pipes[getOffset(from) + to - from - 1].pipes[pipe], &msg->s_header, sizeof(MessageHeader))) == 0 || errno == EAGAIN || errno == EWOULDBLOCK) 
    	{
    		continue;
    	}

	    fflush(pipeslog);
	    break;
    }

    if (i != proc_cnt + 1) 
	{
		local_time++;
	}

    return i;
}
