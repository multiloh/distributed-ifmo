#ifndef __LOGS__H
#define __LOGS__H

#include <stdio.h>

FILE *eventslog; 
FILE *pipeslog;

void logs_open();
void logs_close();

#endif /* __LOGS__H */
