#ifndef __LTIME__H
#define __LTIME__H

#include "ipc.h"

timestamp_t local_time;
timestamp_t r_time;

void time_init();

#endif /* __LTIME__H */
