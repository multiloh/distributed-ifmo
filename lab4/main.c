#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "common.h"
#include "ipc.h"
#include "logs.h"
#include "ltime.h"
#include "pa2345.h"
#include "procs.h"

Message message_build(MessageType type, timestamp_t time, char *payload, int pl_length);

#define nullptr NULL


int mutexl = 0;


static int procs_done = 0;

int request_cs(const void *self) 
{
    int i;

    struct data_t data = *((struct data_t*) self);

    for(i = 0; data.queue[i].id != id; i++);

    data.queue[i].time = get_lamport_time();

    qsort(data.queue, proc_cnt, sizeof(struct queue_t), q_comparator);

    Message req = message_build(CS_REQUEST, local_time, NULL, 0);
    send_multicast(data.pipes, &req);

    int replies = 0;

    while(1) 
    {
        int k;
        int sid;

        memset(&req, 0, sizeof(req));
        sid = receive_any(data.pipes, &req);

        if (sid > proc_cnt && procs_done < proc_cnt - 1) 
        {
            continue;
        }

        if(sid <= proc_cnt)
        {
            if (req.s_header.s_type == DONE)
            {
                procs_done++;
            }
            else if (req.s_header.s_type == CS_REPLY)
            {
                replies++;
            }
            else if (req.s_header.s_type == CS_RELEASE)
            {
                for (k = 0; data.queue[k].id != sid; k++);
                data.queue[k].time = 999;
            }
            else if (req.s_header.s_type == CS_REQUEST)
            {
                for (k = 0; data.queue[k].id != sid; k++);
                data.queue[k].time = req.s_header.s_local_time;

                Message repl = message_build(CS_REPLY, get_lamport_time(), NULL, 0);
                send(data.pipes, sid, &repl);
            }
        }

        qsort(data.queue, proc_cnt, sizeof(struct queue_t), q_comparator);

        if (data.queue[0].id == id && (proc_cnt - 1) <= replies + procs_done) 
        {
            break;
        }
    }

    return 0;
}

int release_cs(const void *self) 
{
    Message msg;

    struct data_t data = *((struct data_t*) self);

    data.queue[0].time = 999;

    qsort(data.queue, proc_cnt, sizeof(struct queue_t), q_comparator);

    msg = message_build(CS_RELEASE, get_lamport_time(), NULL, 0);
    send_multicast(data.pipes, &msg);

    return 0;
}


int main(int argc, char **argv)
{
    time_init();
    logs_open();

    int opt;
    int opt_index = 0;
    int i;
    int j;
    int k;
    int end;
    int iend;
    int jend;
    char *tmp;
    id = PARENT_ID;

    static struct option long_options[] =
    {
        {"p", required_argument, 0, 'p'},
        {"mutexl", no_argument, &mutexl, 1},
        {0, 0, 0, 0}
    };
    
    while((opt = getopt_long (argc, argv, "p:", long_options, &opt_index)) != -1)
    {
        switch (opt) {
            case 'p':
                proc_cnt = strtol(optarg, &tmp, 0);

                if (tmp == optarg || *tmp != '\0') {
                    fprintf(stderr, "-p: Wrong argument!\n");
                    return 1;
                }

                if (proc_cnt < 1) {
                    fprintf(stderr, "Number of processes should be between 1 and 10.\n");
                    return 1;
                }

                break;
            case '?':
                return 1;
        }
    }

    pid_t *children = malloc(sizeof(pid_t) * proc_cnt);
    struct queue_t *queue = malloc(sizeof(struct queue_t) * proc_cnt);

    for (i = 0; i < proc_cnt; i++)
    {
        queue[i].id = i + 1;
        queue[i].time = 999;
    }
  
    struct pipes_t *pipes;
    int pipe_count = 0;

    for (i = 1; i <= proc_cnt; i++)
    {
        pipe_count += i;
    }

    pipes = malloc(sizeof(struct pipes_t) * pipe_count);

    for (i = 0; i < pipe_count; i++) 
    {
        pipe2((pipes + i)->pipes, O_NONBLOCK);
        pipe2((pipes + i)->pipes + 2, O_NONBLOCK);
        fprintf(pipeslog,"Connections created: %d --> %d and %d --> %d\n", pipes[i].pipes[0], pipes[i].pipes[1], pipes[i].pipes[2], pipes[i].pipes[3]);
    }

    fflush(pipeslog);
  
    for (i = 0; i < proc_cnt; i++) 
    {
        children[i] = fork();

        if (children[i] == -1) 
        {
            fprintf(stderr, "Child creation error! i = %d", i);
            return 1;
        }

        if (children[i] == 0) 
        {
            free(children);
            children = NULL;
            id = i + 1;

            break;
        }
    }

    iend = getOffset(proc_cnt - 1);

    for (i = getOffset(id + 1); i <= iend; i++)
    {
        for (j = 0; j < 4; j++)
        {
            if (close(pipes[i].pipes[j]))
            {
                fprintf(stderr, "'Close below' error! i = %d, j = %d", i, j);
                return 1;
            }
        }
    }

    for (i = 0; i < id; i++)
    {
        jend = getOffset(i + 1);

        for (j = getOffset(i); j < jend; j++)
        {
            if (i + j + 1 - getOffset(i) != id)
            {
                for (k = 0; k < 4; k++)
                {
                    if (close(pipes[j].pipes[k]))
                    {
                        fprintf(stderr, "'Close above' error! i = %d, j = %d, k = %d", i, j, k);
                        return 1;
                    }
                }
            }
        }
    }

    for (i = getOffset(id), iend = getOffset(id + 1); i < iend; i++)
    {
        if (close(pipes[i].pipes[1]))
        {
            fprintf(stderr, "'Close your[1]' error! i = %d", i);
            return 1;
        }

        if (close(pipes[i].pipes[2]))
        {
            fprintf(stderr, "'Close your[2]' error! i = %d", i);
            return 1;
        }
    }

    for (i = 0; i < id; i++)
    {
        if (close(pipes[getOffset(i) - i + id - 1].pipes[0]))
        {
            fprintf(stderr, "'Close above[0]' error! i = %d", i);
            return 1;
        }

        if (close(pipes[getOffset(i) - i + id - 1].pipes[3]))
        {
            fprintf(stderr, "'Close above[3]' error! i = %d", i);
            return 1;
        }
    }

    struct data_t data;
    data.pipes = pipes;
    data.queue = queue;
    Message resp;

    char payload[100];
    char buf[256];
    int pl_length;
  
    if(id)
    {
    pl_length = sprintf(payload, log_started_fmt, local_time, id, getpid(), getppid(), 0);

        Message msg = message_build(STARTED, get_lamport_time(), payload, 0);
        send_multicast(pipes, &msg);

        for (i = id != 0; i <= proc_cnt; i++)
        {
            if (i != id) 
            {
                receive(pipes, i, &resp);
            }
        }

        fprintf(eventslog, log_received_all_started_fmt, local_time, id);
        fflush(eventslog);

        for (i = 0, end = id * 5; i < end; i++)
        {
            request_cs(&data);

            memset(buf, 0, strlen(buf));
            sprintf(buf, log_loop_operation_fmt, id, i + 1, end);
            print(buf);

            release_cs(&data);
        }

        fflush(eventslog);

        memset(payload, 0, pl_length);
        pl_length = sprintf(payload, log_done_fmt,local_time, id, 0);

        Message done = message_build(DONE, get_lamport_time(), payload, 0);
        send_multicast(pipes, &done);

        while(procs_done != proc_cnt - 1) 
        {
            int tmp;

            memset(&resp, 0, sizeof(resp));
            tmp = receive_any(pipes, &resp);

            if (proc_cnt < tmp || resp.s_header.s_type != DONE) 
            {
                continue;   
            }

            procs_done++;
        }

        fprintf(eventslog, log_received_all_done_fmt, local_time, id);
        fflush(eventslog);
    }
    else 
    {
        for (i = id != 0; i <= proc_cnt; i++)
        {
            if (i != id) 
            {
                receive(pipes, i, &resp);
            }
        }

        for (i = id != 0; i <= proc_cnt; i++)
        {
            if (i != id) 
            {
                memset(&resp, 0, sizeof(Message));
                receive(pipes, i, &resp);
                if (resp.s_header.s_type != DONE) --i;
            }
        }
    }

    while (wait(NULL) > 0) ;

    logs_close();

    free(pipes);
    free(queue);

    return 0;
}

Message message_build(MessageType type, timestamp_t time, char *payload, int pl_length)
{
    int i;
    Message msg;
  
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_payload_len = pl_length;
    msg.s_header.s_type = type;
    msg.s_header.s_local_time = time;
  
    for (i = 0; i < pl_length; i++) 
    {
        msg.s_payload[i] = payload[i];
    }
  
    return msg;
}
