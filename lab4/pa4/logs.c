#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "common.h"
#include "logs.h"

void logs_open()
{
    eventslog = fopen(events_log, "w");
  	pipeslog = fopen(pipes_log, "w");
}

void logs_close()
{
    fclose(pipeslog);
    fclose(eventslog);
}
