#include "banking.h"
#include "ipc.h"
#include "ltime.h"

timestamp_t get_lamport_time() {
	local_time++;
    return local_time;
}

void time_init() 
{
	local_time = 0;
    r_time = 0;
}
