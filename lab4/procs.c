#include "procs.h"

int getOffset(int proccess)
{
    if (proccess < 0) 
	{ 
		return 0;
	}

    if (proccess == 0) 
	{
    	return 0;
    }

    return getOffset(proccess - 1) - proccess + proc_cnt + 1;
}

int q_comparator(const void *a, const void *b)
{
    struct queue_t q1 = *((struct queue_t*) a);
    struct queue_t q2 = *((struct queue_t*) b);

    if (q1.time > q2.time) 
	{ 
		return 1; 
	}
	else if (q1.time < q2.time)
	{
		return -1;
	}

    if (q1.id > q2.id) 
	{ 
		return 1; 
	}
	else if (q1.id < q2.id)
	{
		return -1;
	}

  	return 0;
}
