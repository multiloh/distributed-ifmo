#ifndef __PROCS__H
#define __PROCS__H

#include "banking.h"
#include "ipc.h"

int id;
int proc_cnt;

struct pipes_t {
    int pipes[4];
};

struct queue_t {
  int id;
  short time;
};

struct data_t {
    struct pipes_t *pipes;
    struct queue_t *queue;
};

int getOffset(int proccess);
int q_comparator(const void *a, const void *b);

#endif /* __PROCS__H */
