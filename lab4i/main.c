#define _GNU_SOURCE
#include "common.h"
#include "ipc.h"
#include "pa2345.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>

#define nullptr NULL

static FILE *eventslog, *pipeslog;
static int id;
static int children_count;
static timestamp_t local_time = 0;
static timestamp_t r_time = 0;
static int mut = 0;

typedef struct pipes_t pipes_t;

struct pipes_t {
  int pipes[4];
};

typedef struct queue_t queue_t;

struct queue_t {
  int id;
  short time;
};

typedef struct data_t data_t;

struct data_t {
    pipes_t *pipes;
    queue_t *queue;
};

timestamp_t get_lamport_time(){
  return ++local_time;
}

static int comp(const void *elem1, const void *elem2) {
  queue_t q1 = *((queue_t*)elem1);
  queue_t q2 = *((queue_t*)elem2);
  if(q1.time > q2.time) return 1;
  if(q1.time < q2.time) return -1;
  if(q1.id > q2.id) return 1;
  if(q1.id < q2.id) return -1;
  return 0;
}

static int dones = 0;

int request_cs(const void *self) {
    data_t d = *((data_t*)self);
    int i = 0;
    for(; d.queue[i].id != id; ++i);
    d.queue[i].time = get_lamport_time();
    qsort(d.queue, children_count, sizeof(queue_t), comp);
    Message req;
    req.s_header.s_local_time = local_time;
    req.s_header.s_magic = MESSAGE_MAGIC;
    req.s_header.s_type = CS_REQUEST;
    req.s_header.s_payload_len = 0;
    send_multicast(d.pipes, &req);
    int replies = 0;
    while(1) {
//	printf("%d->%d(%d + %d)\n", id, replies + dones, replies, dones);
//	printf("%d\n", local_time);
	memset(&req, 0, sizeof(req));
	int sid = receive_any(d.pipes, &req);
	if(sid > children_count && dones < children_count - 1) continue;
	if(sid <= children_count)
	switch(req.s_header.s_type) {
	case DONE: {
	    dones++;
	    break;
	}
	case CS_REPLY: {
	    ++replies;
	    break;
	}
	case CS_RELEASE: {
//	    printf("delete %d %d\n", sid, id);
	    int k = 0;
	    for(; d.queue[k].id != sid; ++k);
	    d.queue[k].time = 999;
	    break;
	}
	case CS_REQUEST: {
//	    printf("add %d %d\n", sid, id);
	    int k = 0;
	    for(; d.queue[k].id != sid; ++k);
	    d.queue[k].time = req.s_header.s_local_time;
	    Message repl;
	    repl.s_header.s_local_time = get_lamport_time();
	    repl.s_header.s_magic = MESSAGE_MAGIC;
	    repl.s_header.s_payload_len = 0;
	    repl.s_header.s_type = CS_REPLY;
	    send(d.pipes, sid, &repl);
	    break;
	}
	}
	qsort(d.queue, children_count, sizeof(queue_t), comp);
	/*printf("%d -> %d\n", id, local_time);
	for(int w = 0; w < children_count; ++w)
	    printf("%d->id: %d; time:%d\n", id, d.queue[w].id, d.queue[w].time);
	    printf("%d->%d(%d + %d)\n", id, replies + dones, replies, dones);
	printf("\n");*/
	if(d.queue[0].id == id && replies + dones >= children_count - 1) break;
    }
    return 0;
}

int release_cs(const void *self) {
    data_t d = *((data_t*)self);
//    d.queue[0].id = 256;
    d.queue[0].time = 999;
    qsort(d.queue, children_count, sizeof(queue_t), comp);
    Message msg;
    msg.s_header.s_local_time = get_lamport_time();
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_payload_len = 0;
    msg.s_header.s_type = CS_RELEASE;
    send_multicast(d.pipes, &msg);
/*    while(1) {
	memset(&msg, 0, sizeof(msg));
	int sid = receive_any(d, &msg);
	if(sid > children_count) break;
	int k = 0;
	for(; d.queue[k] != 256; ++k);
	d.queue[k].id = k;
	d.queue[k].time = msg.s_header.s_local_time;
	qsort(d.queue, children_count, sizeof(queue_t), comp);
	}*/
    return 0;
}

enum {
  SEND_TO_YOURSELF = 1,
  MESSAGE_IS_NULL,
  READ_FROM_YOURSELF
};

static int getOffset(int proccess)
{
  if(proccess < 0) return 0;
  if(proccess == 0) return 0;
  return getOffset(proccess - 1) + children_count - proccess + 1;
}

int send(void *self, local_id dst, const Message *msg)
{
  if(msg == nullptr)
    return MESSAGE_IS_NULL;
  pipes_t *pipes = (pipes_t *)self;
  int from = id, to = dst, pipe_num = 3;
  if(dst == id)
    return SEND_TO_YOURSELF;
  if(dst < id)
    from = dst, to = id, pipe_num = 1;
//  printf("%d to %d\n", id, dst);
  if(write(pipes[getOffset(from) + to - from - 1].pipes[pipe_num], msg, msg->s_header.s_payload_len + sizeof(msg->s_header)) == -1){
     fprintf(stderr, "process %d; pipe: %d === ", id,pipes[getOffset(from) + to - from - 1].pipes[pipe_num]);
    perror("write");
    return -10;
  }
//  printf("%d to %d end\n", id, dst);
//  printf("write '%s' from %d to %d by %d channel;type: %d; len: %d\n", msg->s_payload , id, dst, pipes[getOffset(from) + to - from - 1].pipes[pipe_num], msg->s_header.s_type, msg->s_header.s_payload_len);
  fflush(pipeslog);
  return 0;
}

int send_multicast(void *self, const Message *msg)
{
//    printf("%d: sm: %d\n", id, msg->s_header.s_type);
  int _res = 0;
  for(int i = 0; i <= children_count; ++i)
    if(i != id)
      if ((_res = send(self, i, msg))) return _res;
//  printf("%d: sm end\n", id);
  return _res;
}

int receive(void *self, local_id from, Message *msg)
{
  if (msg == nullptr)
    return MESSAGE_IS_NULL;
  pipes_t *pipes = (pipes_t *)self;
  int _from = from, _to = id, pipe_num = 2;
  if(_from == _to)
    return READ_FROM_YOURSELF;
  if(_from > _to)
    _from = id, _to = from, pipe_num = 0;
  int tmp = 0;
  int readn = 0;
  while((tmp = read(pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], &msg->s_header, sizeof(msg->s_header))) == 0 || errno == 11) errno = 0;
  if(tmp != 0)
      if(tmp == -1 || (readn = read(pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], &msg->s_payload, msg->s_header.s_payload_len)) == -1){

    fprintf(stderr, "process %d; pipe: %d === ", id,pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num]);
    perror("read");
    return -10;
  }  
//  printf("read '%s' from %d to %d by %d channel; type: %d; len: %d\n", msg->s_payload , from, id, pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], msg->s_header.s_type, msg->s_header.s_payload_len);
  fflush(pipeslog);

  if((r_time = msg->s_header.s_local_time) > local_time) local_time = r_time;
  local_time++;
  
  return 0;
}

int receive_any(void *self, Message *msg){
  pipes_t *pipes = (pipes_t *)self;
  int pipe_num = 2, _to = id, _from = 0, i = 0, tmp;
  for(; i < children_count + 1; ++i) {
    if (i == id) continue;
    _from = i;
    if (_from > _to)
	_from = id, _to = i, pipe_num = 0;
    errno = 0;
    tmp = read(pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], &msg->s_header, sizeof(msg->s_header));
    if(errno == 11 || tmp == 0) continue;
    if(tmp < 0) {
      fprintf(stderr, "process %d; pipe: %d === ", id,pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num]);
      perror("read any");
      return -10;
    }
    if(read(pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], &msg->s_payload, msg->s_header.s_payload_len) == -1) {
      fprintf(stderr, "process %d; pipe: %d === ", id,pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num]);
      perror("read any");
      return -10;      
    }
//  printf("read(any) '%s' from %d to %d by %d channel; type: %d; len: %d\n", msg->s_payload , i, id, pipes[getOffset(_from) + _to - _from - 1].pipes[pipe_num], msg->s_header.s_type, msg->s_header.s_payload_len);
    fflush(pipeslog);
    break;
  }
  if(i != (children_count + 1)) ++local_time;
  return i;
}

int main(int argc, char **argv)
{
  char flag;
  id = PARENT_ID;
  int flag_exists = 0;
  eventslog = fopen(events_log, "w");
  pipeslog = fopen(pipes_log, "w");
  static struct option long_options[] =
  {
      /* These options set a flag. */
      {"mutexl", no_argument,       &mut, 1},
      /* These options don’t set a flag.
	 We distinguish them by their indices. */
      {"p",     required_argument,       0, 'p'},
      {0, 0, 0, 0}
  };
  int option_index = 0;
  while((flag = getopt_long (argc, argv, "p:",
			     long_options, &option_index)) != -1)
    switch(flag){
    case 0:
	break;
    case 'p':
      for (int i = 0, length = strlen(optarg); i < length; i++)
	if (!isdigit(optarg[i])) {
	  fprintf (stderr, "Entered argument is not a number\n");
	  exit(1);
	}
      children_count = atoi(optarg);
      if(children_count > MAX_PROCESS_ID || children_count <= PARENT_ID) {
	fprintf (stderr, "Entered argument in invalid(1..15)\n");
	exit(1);	    
      }
      flag_exists = 1;
      break;
    case '?':
      exit(1);
    }
  if(!flag_exists) {
    fprintf(stderr, "flag -p is missing\n");
    exit(1);
  }
  pid_t *children = malloc(sizeof(pid_t) * children_count);
  queue_t *queue = malloc(sizeof(queue_t) * children_count);
  for(int i = 0; i < children_count; ++i)
    queue[i].id = i+1, queue[i].time = 999;
  
  pipes_t *pipes;
  int pipe_count = 0;
  for(int i = 1; i <= children_count; ++i)
    pipe_count += i;
  pipes = malloc(sizeof(pipes_t) * pipe_count);
  for(int i = 0; i < pipe_count; ++i) {
      pipe2((pipes+i)->pipes, O_NONBLOCK);
      pipe2((pipes+i)->pipes + 2, O_NONBLOCK);
      fprintf(pipeslog,"open pipes %d-%d and %d-%d\n", pipes[i].pipes[0], pipes[i].pipes[1], pipes[i].pipes[2], pipes[i].pipes[3]);
  }
  fflush(pipeslog);
  
  for(int i = 0; i < children_count; ++i) {
    if ((children[i] = fork()) == 0) {
      free(children);
      children = nullptr;
      id = i + 1;
      break;
    }
    if(children[i] == -1) {
      perror("fork()");
      exit(1);
    }
  }
  //close fd
  //close a connections that not belong to you
  for(int i = getOffset(id + 1), iend = getOffset(children_count - 1); i <= iend ;++i)
    for(int j = 0; j < 4; ++j)
      if(close(pipes[i].pipes[j]))
	perror("close below");
  for(int i = 0; i < id; ++i)
    for(int j = getOffset(i), jend = getOffset(i + 1); j < jend; ++j)
      if(i + j + 1 - getOffset(i) != id)
	for(int k = 0; k < 4; ++k)
	  if(close(pipes[j].pipes[k]))
	     perror("close above");
  //close one end
  //in your row
  for(int i = getOffset(id), iend = getOffset(id + 1); i < iend; ++i){
    if(close(pipes[i].pipes[1]))
      perror("your[1]");
    if(close(pipes[i].pipes[2]))
      perror("your[2]");
  }
  //in rows above
  for(int i = 0; i < id; ++i){
    if(close(pipes[getOffset(i) - i + id - 1].pipes[0]))
      perror("above[0]");
    if(close(pipes[getOffset(i) - i + id - 1].pipes[3]))
      perror("above[3]");
  }

  data_t data;
  data.pipes = pipes;
  data.queue = queue;
  
  if(id){
    //start
    Message msg;
    msg.s_header.s_local_time = get_lamport_time();
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = STARTED;
    sprintf(msg.s_payload,log_started_fmt, local_time, id, getpid(), getppid(), 0);
    msg.s_payload[strlen(msg.s_payload) - 1] = 0;
    msg.s_header.s_payload_len = strlen(msg.s_payload) + 1;
    send_multicast(pipes, &msg);
    for (int i = id != 0; i <= children_count; ++i)
      if(i != id) {
	Message rcv;
	receive(pipes, i, &rcv);
      }
    fprintf(eventslog, log_received_all_started_fmt, local_time, id);
    fflush(eventslog);
    //work
    for(int i = 0, end = id * 5; i < end; ++i){
	request_cs(&data);
	char buf[256];
	sprintf(buf, log_loop_operation_fmt, id, i + 1, end);
	print(buf);
	release_cs(&data);
    }
    //done
    fflush(eventslog);
    Message done;
    done.s_header.s_local_time = get_lamport_time();
    done.s_header.s_magic = MESSAGE_MAGIC;
    done.s_header.s_type = DONE;
    sprintf(done.s_payload, log_done_fmt,local_time, id, 0);
    done.s_payload[strlen(done.s_payload) - 1] = 0;
    done.s_header.s_payload_len = strlen(done.s_payload) + 1;
    send_multicast(pipes, &done);
    Message rcv;
//    printf("%d->%d", id, dones);
    while(dones != children_count - 1) {
	if (receive_any(pipes, &rcv) > children_count) continue;
	if (rcv.s_header.s_type != DONE) continue;
	memset(&rcv, 0, sizeof(rcv));
	dones++;
//	printf("%d->%d", id, dones);
    }
    fprintf(eventslog, log_received_all_done_fmt, local_time, id);
    fflush(eventslog);
  }
  else {
    //start
    for (int i = id != 0; i <= children_count; ++i)
      if(i != id) {
	Message rcv;
	receive(pipes, i, &rcv);
      }
    //get DONE
    for (int i = id != 0; i <= children_count; ++i)
      if(i != id) {
	Message rcv_done;
	memset(&rcv_done, 0, sizeof(Message));
	receive(pipes, i, &rcv_done);
	if(rcv_done.s_header.s_type != DONE) --i;
      }
  }
  if(children != nullptr) {
    for(int i = 0; i < children_count; ++i)
	wait(children + i);
    free(children);
  }
  fclose(eventslog);
  fclose(pipeslog);
  free(pipes);
  free(queue);
  return 0;
}
